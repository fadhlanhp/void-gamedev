extends KinematicBody2D

export (int) var speed = 400
export (int) var GRAVITY = 1200
export (int) var jump_speed = -530

const UP = Vector2(0,-1)

var velocity = Vector2()

#onready var animator = self.get_node("Animator")
#onready var sprite = self.get_node("Sprite")

func _physics_process(delta):
	velocity.x = 0
	if is_on_floor() and Input.is_action_just_pressed('ui_up'):
		velocity.y = jump_speed
	if Input.is_action_pressed('ui_right'):
		velocity.x += speed
	if Input.is_action_pressed('ui_left'):
		velocity.x -= speed
	velocity.y += delta * GRAVITY
	velocity = move_and_slide(velocity, UP)
