extends RigidBody2D

var picked = false

func _physics_process(delta):
	if picked == true:
		self.position = get_node("../StarSailor/Position2D").global_position
		#print(self.position)
		sleeping = true
	else:
		sleeping = false

func _input(event):
	if Input.is_action_just_pressed("pick_up"):
		if global.pick_counter == 0:
			var bodies = $Detector.get_overlapping_bodies()
			for b in bodies:
				if b.name == "StarSailor" and picked == false:
					picked = true
					global.pick_counter = 1
	if Input.is_action_just_pressed("drop") and picked == true:
		picked = false
		global.pick_counter = 0
